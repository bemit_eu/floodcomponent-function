<?php

namespace Flood\Component\Func;


class Array_ {
    /**
     * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
     * keys to arrays rather than overwriting the value in the first array with the duplicate
     * value in the second array, as array_merge does. I.e., with array_merge_recursive,
     * this happens (documented behavior):
     *
     * array_merge_recursive(
     *      ['key' => 'org value'],
     *      ['key' => 'new value']
     * );
     *
     * result:
     * ['key' => [
     *      'org value',
     *      'new value'
     * ]]
     *
     * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
     * Matching keys' values in the second array overwrite those in the first array, as is the
     * case with array_merge, i.e.:
     *
     * Flood\Component\Func\Array_::merge_recursive_distinct(
     *      ['key' => 'org value'],
     *      ['key' => 'new value']
     * );
     *
     * result:
     * ['key' => ['new value']]
     *
     * @param array $array1
     * @param array $array2
     *
     * @return array
     * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
     * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
     */
    public static function merge_recursive_distinct(array $array1, array $array2) {
        $merged = $array1;

        foreach($array2 as $key => &$value) {
            if(is_array($value) && isset ($merged[$key]) && is_array($merged[$key])) {
                $merged[$key] = static::merge_recursive_distinct($merged[$key], $value);
            } else {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }
}